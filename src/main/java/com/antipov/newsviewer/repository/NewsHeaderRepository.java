package com.antipov.newsviewer.repository;

import com.antipov.newsviewer.entity.NewsHeader;
import com.antipov.newsviewer.domain.WebSource;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

public interface NewsHeaderRepository extends CrudRepository<NewsHeader, Long> {

    List<NewsHeader> findAllByDateAndWebSource(final Date date, final WebSource webSource);
}
