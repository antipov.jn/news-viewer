package com.antipov.newsviewer.controller;

import com.antipov.newsviewer.application.NewsParserUcs;
import com.antipov.newsviewer.domain.WebSource;
import com.antipov.newsviewer.dto.NewsHeaderDto;
import com.antipov.newsviewer.exception.WebResourceNotSupportException;
import com.antipov.newsviewer.exception.WebResourceUnavailableException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;


@Controller
@RequestMapping("/news")
public class NewsController {

    private final NewsParserUcs extractNewsHeaders;

    public NewsController(final NewsParserUcs newsParserUcs) {
        this.extractNewsHeaders = newsParserUcs;
    }

    @GetMapping
    public String getWebSourceList(final Model model) {
        model.addAttribute("webSources", WebSource.values());
        return "source_list";
    }

    @GetMapping("/{webSourceStr}")
    public String getNewsList(@PathVariable final String webSourceStr, final Model model) {
        final WebSource webSource = parseWebSource(webSourceStr);
        try {
           final List<NewsHeaderDto> newsHeaders = extractNewsHeaders.extractAndMaybeSaveCurrentDateNews(webSource);

            model.addAttribute("newsList", newsHeaders);
            model.addAttribute("title", webSourceStr.toLowerCase());

            return "news_list";
        } catch (IOException | ParseException e) {
            throw new WebResourceUnavailableException(webSource);
        }

    }

    private WebSource parseWebSource(String webSourceStr) {
        try {
            return WebSource.valueOf(webSourceStr.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new WebResourceNotSupportException(webSourceStr);
        }
    }
}
