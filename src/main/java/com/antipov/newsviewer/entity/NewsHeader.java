package com.antipov.newsviewer.entity;

import com.antipov.newsviewer.domain.WebSource;
import com.antipov.newsviewer.dto.NewsHeaderDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "news_header")
@Data
@EqualsAndHashCode(of = "id")
@NoArgsConstructor
public class NewsHeader {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String value;
    @Enumerated(value = EnumType.STRING)
    private WebSource webSource;
    @Temporal(TemporalType.DATE)
    private Date date;

    public NewsHeader(final String value, final WebSource webSource, final Date date) {
        this.value = value;
        this.webSource = webSource;
        this.date = date;
    }

    public NewsHeader(final NewsHeaderDto newsHeaderDto){
        this.value = newsHeaderDto.getValue();
        this.webSource = newsHeaderDto.getWebSource();
        this.date = newsHeaderDto.getDate();
    }

    public NewsHeaderDto toDto(){
        return new NewsHeaderDto(value, webSource, date);
    }
}
