package com.antipov.newsviewer.dto;

import com.antipov.newsviewer.domain.WebSource;
import lombok.Data;

import java.util.Date;

@Data
public class NewsHeaderDto {

    private final String value;
    private final WebSource webSource;
    private final Date date;
}
