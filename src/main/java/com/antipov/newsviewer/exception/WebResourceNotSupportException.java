package com.antipov.newsviewer.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "The web resource is not supports")
public class WebResourceNotSupportException extends RuntimeException {

    public WebResourceNotSupportException(final String resourceName) {
        super(String.format("'%s' web resource is not supports", resourceName));
    }
}
