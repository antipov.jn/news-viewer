package com.antipov.newsviewer.exception;

import com.antipov.newsviewer.domain.WebSource;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_IMPLEMENTED)
public class ClassNotImplementedException extends RuntimeException {

    public ClassNotImplementedException(final Class<?> clazz, final WebSource webSource) {
        super(String.format("%s for '%s' is not implemented", clazz.getName(), webSource.toString()));
    }
}
