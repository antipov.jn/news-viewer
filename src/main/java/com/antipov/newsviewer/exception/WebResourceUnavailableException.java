package com.antipov.newsviewer.exception;

import com.antipov.newsviewer.domain.WebSource;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.SERVICE_UNAVAILABLE, reason = "The web resource is unavailable")
public class WebResourceUnavailableException extends RuntimeException{

    public WebResourceUnavailableException(final WebSource webSource) {
        super(String.format("'%s' web resource is unavailable", webSource.name()));
    }
}
