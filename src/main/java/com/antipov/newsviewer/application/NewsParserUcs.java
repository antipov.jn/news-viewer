package com.antipov.newsviewer.application;

import com.antipov.newsviewer.dto.NewsHeaderDto;
import com.antipov.newsviewer.entity.NewsHeader;
import com.antipov.newsviewer.domain.WebSource;
import com.antipov.newsviewer.exception.ClassNotImplementedException;
import com.antipov.newsviewer.repository.NewsHeaderRepository;
import com.antipov.newsviewer.service.RelatedSource;
import com.antipov.newsviewer.service.newsparser.AbstractNewsParserService;
import com.antipov.newsviewer.service.newsparser.NewsParserService;
import com.antipov.newsviewer.service.pageloader.AbstractPageLoaderService;
import com.antipov.newsviewer.service.pageloader.PageLoaderService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class NewsParserUcs {

    private final List<AbstractPageLoaderService> pageLoaderServices;
    private final List<AbstractNewsParserService> parserServices;
    private final NewsHeaderRepository newsHeaderRepository;
    private final Boolean returnFromDatabase;

    public NewsParserUcs(final List<AbstractPageLoaderService> pageLoaderServices,
                         final List<AbstractNewsParserService> parserServices,
                         final NewsHeaderRepository newsHeaderRepository,
                         final @Value("${newsparser.returnFromDatabase}") Boolean returnFromDatabase) {
        this.pageLoaderServices = pageLoaderServices;
        this.parserServices = parserServices;
        this.newsHeaderRepository = newsHeaderRepository;
        this.returnFromDatabase = returnFromDatabase;
    }

    public List<NewsHeaderDto> extractAndMaybeSaveCurrentDateNews(final WebSource webSource) throws ParseException, IOException {
        final List<NewsHeader> todaySavedHeaders = findAllNewsHeadersByWebSourceAndCurrentDate(webSource);
        if (!returnFromDatabase || todaySavedHeaders.isEmpty()) {
            return mapNewsHeadersToDto(extractAndSaveUniqueNewsHeaders(todaySavedHeaders, webSource));
        }
        return mapNewsHeadersToDto(todaySavedHeaders);
    }

    private List<NewsHeader> extractAndSaveUniqueNewsHeaders(final List<NewsHeader> todaySavedHeaders,
                                                             final WebSource webSource) throws IOException {
        final AbstractPageLoaderService pageLoaderService = findPageLoaderByWebSource(webSource);
        final AbstractNewsParserService newsParserService = findParserServiceByWebSource(webSource);

        final List<NewsHeader> extractedNewsHeaders = extractNewsHeaders(pageLoaderService, newsParserService);
        final List<NewsHeader> uniqueNewsHeaders = filterUniqueNewsHeaders(todaySavedHeaders, extractedNewsHeaders);

        newsHeaderRepository.saveAll(uniqueNewsHeaders);

        return Stream.concat(todaySavedHeaders.stream(), uniqueNewsHeaders.stream())
                .collect(Collectors.toList());
    }

    private List<NewsHeader> extractNewsHeaders(final AbstractPageLoaderService pageLoaderService,
                                                final AbstractNewsParserService newsParserService) throws IOException {
        return newsParserService.extractNewsHeaders(pageLoaderService.download());
    }

    private List<NewsHeader> filterUniqueNewsHeaders(final List<NewsHeader> todaySavedHeaders,
                                                     final List<NewsHeader> extractedNewsHeaders) {
        return extractedNewsHeaders.stream().filter(h -> !todaySavedHeaders.contains(h)).collect(Collectors.toList());
    }

    private List<NewsHeader> findAllNewsHeadersByWebSourceAndCurrentDate(final WebSource webSource) throws ParseException {
        String currentDateStr = new SimpleDateFormat("yyyy.MM.dd").format(new Date(System.currentTimeMillis()));
        Date currentDate = new SimpleDateFormat("yyyy.MM.dd").parse(currentDateStr);
        return newsHeaderRepository.findAllByDateAndWebSource(currentDate, webSource);
    }

    private Predicate<RelatedSource> getRequiredSourcePredicate(final WebSource webSource) {
        return ps -> ps.getSource().equals(webSource);
    }


    private AbstractNewsParserService findParserServiceByWebSource(final WebSource webSource) {
        return parserServices.stream()
                .filter(getRequiredSourcePredicate(webSource))
                .findFirst()
                .orElseThrow(() -> new ClassNotImplementedException(NewsParserService.class, webSource));
    }

    private AbstractPageLoaderService findPageLoaderByWebSource(final WebSource webSource) {
        return pageLoaderServices.stream()
                .filter(getRequiredSourcePredicate(webSource))
                .findFirst()
                .orElseThrow(() -> new ClassNotImplementedException(PageLoaderService.class, webSource));
    }

    private List<NewsHeaderDto> mapNewsHeadersToDto(final List<NewsHeader> newsHeaders) {
        return newsHeaders.stream()
                .map(NewsHeader::toDto)
                .collect(Collectors.toList());
    }
}
