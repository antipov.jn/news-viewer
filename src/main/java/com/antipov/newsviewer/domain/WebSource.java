package com.antipov.newsviewer.domain;

public enum WebSource {

    MIRKNIG("mirknig"),
    OPENNET("opennet");

    private final String name;

    WebSource(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
