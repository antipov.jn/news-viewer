package com.antipov.newsviewer.service;

import com.antipov.newsviewer.domain.WebSource;

public interface RelatedSource {

    WebSource getSource();
}
