package com.antipov.newsviewer.service.newsparser;

import com.antipov.newsviewer.domain.WebSource;
import com.antipov.newsviewer.service.RelatedSource;

public abstract class AbstractNewsParserService implements NewsParserService, RelatedSource {

    private final WebSource webSource;

    public AbstractNewsParserService(final WebSource webSource) {
        this.webSource = webSource;
    }

    @Override
    public WebSource getSource() {
        return webSource;
    }
}
