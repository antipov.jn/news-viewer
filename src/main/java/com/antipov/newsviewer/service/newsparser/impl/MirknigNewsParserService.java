package com.antipov.newsviewer.service.newsparser.impl;

import com.antipov.newsviewer.entity.NewsHeader;
import com.antipov.newsviewer.domain.WebSource;
import com.antipov.newsviewer.service.newsparser.AbstractNewsParserService;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class MirknigNewsParserService extends AbstractNewsParserService {

    private final String regexp;

    public MirknigNewsParserService(@Value("${parser.mirknig.regexp}") final String regexp) {
        super(WebSource.MIRKNIG);
        this.regexp = regexp;
    }

    @Override
    public List<NewsHeader> extractNewsHeaders(final Document document) {
        String cssQuery = String.format("table:matches(%s) td.menu1 td[align=left] a", decodeRegexFromIsoToUtf());
        return document.body()
                .select(cssQuery)
                .stream()
                .map(parseHeaders())
                .collect(Collectors.toList());
    }

    private Function<Element, NewsHeader> parseHeaders() {
        return e -> new NewsHeader(e.getElementsByTag("a").first().text(), this.getSource(),  new Date(System.currentTimeMillis()));
    }

    private String decodeRegexFromIsoToUtf() {
        return new String(regexp.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
    }
}
