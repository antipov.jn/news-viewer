package com.antipov.newsviewer.service.newsparser;

import com.antipov.newsviewer.entity.NewsHeader;
import org.jsoup.nodes.Document;

import java.util.List;

public interface NewsParserService {

    List<NewsHeader> extractNewsHeaders(final Document document);
}
