package com.antipov.newsviewer.service.newsparser.impl;

import com.antipov.newsviewer.entity.NewsHeader;
import com.antipov.newsviewer.domain.WebSource;
import com.antipov.newsviewer.service.newsparser.AbstractNewsParserService;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class OpennetNewsParserService extends AbstractNewsParserService {

    public OpennetNewsParserService() {
        super(WebSource.OPENNET);
    }

    @Override
    public List<NewsHeader> extractNewsHeaders(final Document document) {
        final String currentDateRegexp = new SimpleDateFormat("dd.MM.yyyy").format(new Date());
        final String cssQuery = String.format("tr:has(td.tdate):contains(%s) a.title2", currentDateRegexp);

        return document.select(cssQuery)
                .stream()
                .map(e -> new NewsHeader(e.text(), this.getSource(), new Date(System.currentTimeMillis())))
                .collect(Collectors.toList());
    }
}
