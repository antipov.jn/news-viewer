package com.antipov.newsviewer.service.pageloader.impl;

import com.antipov.newsviewer.domain.WebSource;
import com.antipov.newsviewer.service.pageloader.AbstractPageLoaderService;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
@Service

public class OpennetPageLoaderService extends AbstractPageLoaderService {

    protected OpennetPageLoaderService(@Value("${parser.opennet.url}") final String resourceUrl) {
        super(resourceUrl, WebSource.OPENNET);
    }

    @Override
    public Document download() throws IOException {
        return Jsoup.connect(resourceUrl).get();
    }

}
