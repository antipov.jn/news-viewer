package com.antipov.newsviewer.service.pageloader;

import org.jsoup.nodes.Document;

import java.io.IOException;

public interface PageLoaderService {
    Document download() throws IOException;
}
