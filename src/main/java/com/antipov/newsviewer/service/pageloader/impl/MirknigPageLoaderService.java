package com.antipov.newsviewer.service.pageloader.impl;

import com.antipov.newsviewer.domain.WebSource;
import com.antipov.newsviewer.service.pageloader.AbstractPageLoaderService;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class MirknigPageLoaderService extends AbstractPageLoaderService {

    private final String userAgent;

    protected MirknigPageLoaderService(@Value("${parser.mirknig.url}") final String resourceUrl,
                                       @Value("${parser.mirknig.useragent}") final String userAgent) {
        super(resourceUrl, WebSource.MIRKNIG);
        this.userAgent = userAgent;
    }

    @Override
    public Document download() throws IOException {
        return Jsoup.connect(resourceUrl)
                .userAgent(userAgent)
                .get();
    }

}
