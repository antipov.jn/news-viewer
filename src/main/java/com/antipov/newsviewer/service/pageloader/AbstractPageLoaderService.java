package com.antipov.newsviewer.service.pageloader;

import com.antipov.newsviewer.domain.WebSource;
import com.antipov.newsviewer.service.RelatedSource;

public abstract class AbstractPageLoaderService implements PageLoaderService, RelatedSource {

    protected final String resourceUrl;

    protected final WebSource webSource;

    protected AbstractPageLoaderService(final String resourceUrl, final WebSource webSource) {
        this.resourceUrl = resourceUrl;
        this.webSource = webSource;
    }

    @Override
    public WebSource getSource() {
        return webSource;
    }
}
