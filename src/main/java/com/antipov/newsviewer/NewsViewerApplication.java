package com.antipov.newsviewer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
public class NewsViewerApplication {

	public static void main(String[] args) {
		SpringApplication.run(NewsViewerApplication.class, args);
	}

}
